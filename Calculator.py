
# Calculator cu limita de introducere numere, interval acceptat 0-9
while True:
    numar_unu = float(input("Introdu primul numar de la 0 la 9: "))
    if numar_unu >= 0 and numar_unu <= 9:
        operator = input("Introdu +, -, *, / aici: ")
        # Adunare
        if operator == "+":
            numar_doi = float(input("Introdu al doilea numar de la 0 la 9: "))
            while numar_doi < 0 or numar_doi > 9:
                print("Al doilea numar introdus nu este in intervalul 0-9!")
                numar_doi = float(input("Introdu din nou al doilea numar: "))
            else:
                print(numar_unu + numar_doi)

        # Scadere
        elif operator == "-":
            numar_doi = float(input("Introdu al doilea numar de la 0 la 9: "))
            while numar_doi < 0 or numar_doi > 9:
                print("Al doilea numar introdus nu este in intervalul 0-9!")
                numar_doi = float(input("Introdu din nou al doilea numar: "))
            else:
                print(numar_unu - numar_doi)

        # Inmultire
        elif operator == "*":
            numar_doi = float(input("Introdu al doilea numar de la 0 la 9: "))
            while numar_doi < 0 or numar_doi > 9:
                print("Al doilea numar introdus nu este in intervalul 0-9!")
                numar_doi = float(input("Introdu din nou al doilea numar: "))
            else:
                print(numar_unu * numar_doi)

        # Impartire
        elif operator == "/":
            numar_doi = float(input("Introdu al doilea numar de la 0 la 9: "))
            while numar_doi < 0 or numar_doi > 9:
                print("Al doilea numar introdus nu este in intervalul 0-9!")
                numar_doi = float(input("Introdu din nou al doilea numar: "))
            else:
                print(numar_unu / numar_doi)

        # Stergere
        elif operator == "c" or operator == "C":
            del numar_unu
            del operator

        # Eroare operator invalid
        else:
            print("Operator invalid!")

    # Daca numarul ales nu este in intervalul 0-9
    elif numar_unu < 0 or numar_unu > 9:
        print("Primul numar introdus nu apartine intervalului 0-9!")





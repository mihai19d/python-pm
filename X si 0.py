import random
piese = ("X", "0")
alegere = random.choice(piese)


# Piesa primului jucator
def user1():
    player_1 = alegere
    return player_1


# Piesa celui de-al doilea jucator
def user2():
    if user1() == "X":
        player_2 = "0"
    else:
        player_2 = "X"
    return player_2


# Construim tabla de joc
a = [["|___|", "|___|", "|___|"],
    ["|___|", "|___|", "|___|"],
    ["|___|", "|___|", "|___|"]]


# Definim functie pentru rularea tablei de joc
def rulare():
    for fiecare in a:
        print(''.join(fiecare))


# Definim inputul primului jucator
def player1():
    try:
        nr1 = int(input("Player 1 introdu nr: "))
        if 0 < nr1 < 4 and (a[0][nr1 - 1]) == "|___|":
            a[0][nr1 - 1] = "|_" + user1() + "_|"
        elif 3 < nr1 < 7 and (a[1][nr1 - 4]) == "|___|":
            a[1][nr1 - 4] = "|_" + user1() + "_|"
        elif 6 < nr1 < 10 and (a[2][nr1 - 7]) == "|___|":
            a[2][nr1 - 7] = "|_" + user1() + "_|"
        elif nr1 < 1 or nr1 > 9:
            print("Numarul nu se afla in casuta! Incearca din nou")
            player1()
        else:
            print("Locatia este ocupata! Alege alta pozitie!")
            player1()
    except ValueError:
        print("Nu ai introdus o cifra!")
        player1()


# Definim inputul celui de-al doilea jucator
def player2():
    try:
        nr2 = int(input("Player 2 introdu nr: "))
        if 0 < nr2 < 4 and (a[0][nr2 - 1]) == "|___|":
            a[0][nr2 - 1] = "|_" + user2() + "_|"
        elif 3 < nr2 < 7 and (a[1][nr2 - 4]) == "|___|":
            a[1][nr2 - 4] = "|_" + user2() + "_|"
        elif 6 < nr2 < 10 and (a[2][nr2 - 7]) == "|___|":
            a[2][nr2 - 7] = "|_" + user2() + "_|"
        elif nr2 < 1 or nr2 > 9:
            print("Numarul nu se afla in casuta!")
            player2()
        else:
            print("Locatia este ocupata! Alege alta pozitie!")
            player2()
    except ValueError:
        print("Nu ai introdus o cifra!")
        player2()


# Verificam cine castiga sau daca se termina la egalitate
def verificare():
    usr1 = "|_" + user1() + "_|"
    usr2 = "|_" + user2() + "_|"
    count = a[0].count("|___|") + a[1].count("|___|") + a[2].count("|___|")
    if a[0][0] == a[1][0] == a[2][0] == usr1 or a[0][0] == a[1][0] == a[2][0] == usr2:
        print("Felicitari!Ai castigat pe prima coloana!")
        return True
    elif [a[0][1] == a[1][1] == a[2][1]] == usr1 or [a[0][1] == a[1][1] == a[2][1]] == usr2:
        print("Felicitari!Ai castigat pe a doua coloana!")
        return True
    elif a[0][2] == a[1][2] == a[2][2] == usr1 or a[0][2] == a[1][2] == a[2][2] == usr2:
        print("Felicitari!Ai castigat pe a treia coloana!")
        return True
    elif a[0][0] == a[0][1] == a[0][2] == usr1 or a[0][0] == a[0][1] == a[0][2] == usr2:
        print("Felicitari!Ai castigat pe prima linie!")
        return True
    elif a[1][0] == a[1][1] == a[1][2] == usr1 or a[1][0] == a[1][1] == a[1][2] == usr2:
        print("Felicitari!Ai castigat pe a doua linie!")
        return True
    elif a[2][0] == a[2][1] == a[2][2] == usr1 or a[2][0] == a[2][1] == a[2][2] == usr2:
        print("Felicitari!Ai castigat pe a treia linie!")
        return True
    elif a[0][0] == a[1][1] == a[2][2] == usr1 or a[0][0] == a[1][1] == a[2][2] == usr2:
        print("Felicitari!Ai castigat pe diagonala din stanga!")
        return True
    elif a[0][2] == a[1][1] == a[2][0] == usr1 or a[0][2] == a[1][1] == a[2][0] == usr2:
        print("Felicitari!Ai castigat pe diagonala din dreapta!")
        return True
    elif count < 1:
        print("Egalitate!")
        return True


# Incepere joc
def start_joc():
    while True:
        if user1() == "X":
            player1()
            rulare()
            if verificare() is True:
                break
            player2()
            rulare()
            if verificare() is True:
                break
        else:
            player2()
            rulare()
            if verificare() is True:
                break
            player1()
            rulare()
            if verificare() is True:
                break


start_joc()
